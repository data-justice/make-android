package mx.rmr.make.plan

import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.ListFragment
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import com.androidnetworking.interfaces.OkHttpResponseListener
import kotlinx.android.synthetic.main.activity_estado_isla.*
import mx.rmr.make.*
import mx.rmr.make.botonpanico.ListenerFragmentoLista
import mx.rmr.make.islas.ListaCadenasFrag
import mx.rmr.make.util.Comun
import okhttp3.Response
import org.json.JSONException
import org.json.JSONObject

class EstadoIslaActiv : AppCompatActivity(), ListenerFragmentoLista
{
    private var islaEstable: Boolean = false
    private lateinit var dIsla: JSONObject
    private lateinit var nombreIsla: String
    private var idIsla: Int = 0

    private var aliasActual = ""

    private var listaMiembros = mutableListOf<String>()

    private val CODIGO_CAPTURA_PESOS = 300

    private val mapaModelo = mapOf("democratic" to "Democrático",
        "centralized" to "Centralizado", "weighted" to "Ponderado")

    private val modelosIngles = mapOf("Democrático" to "democratic",
        "Ponderado" to "weighted", "Centralizado" to "centralized")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_estado_isla)

        val strDIsla = intent.getStringExtra(LLAVE_JSON_ISLA) ?: "{}"
        dIsla = JSONObject(strDIsla)
        //println("Original $dIsla")
        nombreIsla = dIsla.getString("name")
        idIsla = dIsla.getInt("id")
        tvTituloEstado.text = nombreIsla

        descargarMiembros()
    }

    private fun descargarMiembros() {
        //pbEsperaEstado.visibility = View.VISIBLE
        habilitarEntrada(false)

        val preferencias = getSharedPreferences(PREFS_CREDENCIALES, Context.MODE_PRIVATE)
        val token = preferencias.getString(LLAVE_TOKEN, NO_EXISTE)
        aliasActual = preferencias.getString(LLAVE_ALIAS, "") ?: ""

        val direccion = "$URL_LISTA_GRUPOS/$idIsla"

        AndroidNetworking.get(direccion)
            .addHeaders("Content-type","application/json; charset=utf-8")
            .addHeaders("Authorization", "Bearer $token")
            .build()
            .getAsJSONObject(object: JSONObjectRequestListener {
                override fun onResponse(response: JSONObject?) {
                    //pbEsperaEstado.visibility = View.INVISIBLE
                    habilitarEntrada(true)
                    configurarEstadoIsla(response)
                }

                override fun onError(anError: ANError?) {
                    //pbEsperaEstado.visibility = View.INVISIBLE
                    habilitarEntrada(true)
                    if (anError != null) {
                        when (anError.errorCode) {
                            ERROR_INFO_INVALIDA -> {
                                Comun.mostrarMensaje(
                                    this@EstadoIslaActiv, MENSAJE_CREDENCIALES_INVALIDAS, true, true)
                            }
                            ERROR_NO_INTERNET -> {
                                Comun.mostrarMensaje(
                                    this@EstadoIslaActiv, MENSAJE_NO_INTERNET
                                )
                            }
                            else -> {
                                Comun.mostrarMensaje(
                                    this@EstadoIslaActiv, MENSAJE_ERROR_GENERAL
                                )
                            }
                        }
                    }
                }
            })
    }

    private fun configurarEstadoIsla(response: JSONObject?) {
        listaMiembros.clear()

        val arrMiembros = response?.getJSONArray("members")
        for (i in 0 until arrMiembros!!.length()) {
            val dMiembro = arrMiembros.getJSONObject(i)
            val alias = dMiembro.getString("username")
            listaMiembros.add(alias)
        }
        crearAdaptadorListaMiembros()

        var ndPlanSeguridad: JSONObject? = null
        var nestaEstable: Boolean? = null
        try {
            ndPlanSeguridad = response.getJSONObject("active_security_plan")

        } catch (e: JSONException) {
        }

        try {
            nestaEstable = response.getBoolean("stable")
        } catch (e: JSONException) {
        }

        if (nestaEstable==null || ndPlanSeguridad==null) {
            tvEstado.text = "Estado: No hay plan de seguridad"
            tvModelo.text = "Modelo de gobernanza: NA"
            return
        }

        val estaEstable = nestaEstable!!
        val dPlanSeguridad = ndPlanSeguridad!!

        if (estaEstable) {
            islaEstable = true
            tvEstado.text = "Estado: Estable"
            val modelo = response.getString("governance_model")
            tvModelo.text = "Modelo de gobernanza: ${mapaModelo[modelo]}"
            if (modelo=="centralized") {
                val usuarioDecisiones = response.getString("centralized_user")
                tvModelo.append(" ($usuarioDecisiones)")
            }
        } else {
            islaEstable = false
            if (dPlanSeguridad!!.has("missing_person")) {
                val plan = dPlanSeguridad!!.getString("missing_person")
                tvEstado.text = "Persona desaparecida: $plan"
            } else {
                tvEstado.text = "Estado actual no disponible"
            }
            val dModeloGob = dPlanSeguridad.getJSONObject("governance_details")
            val modeloGob = dModeloGob.getString("winner")
            tvModelo.text = "Modelo de gobernanza: ${mapaModelo[modeloGob]}"
            if (modeloGob=="centralized") {
                val usuarioDecisiones = response.getString("centralized_user")
                tvModelo.append(" ($usuarioDecisiones)")
            }
        }
    }

    private fun crearAdaptadorListaMiembros() {
        val frag = fragListaMiembros as ListaCadenasFrag
        frag.cambiarAdaptador(listaMiembros)
    }

    override fun longClickOnIndex(posicion: Int, fragmento: ListFragment?) {
        // Borrar alias de la lista
        val aliasBorrar = listaMiembros[posicion]
        val preferencias = getSharedPreferences(PREFS_CREDENCIALES, Context.MODE_PRIVATE)
        val aliasActual = preferencias.getString(LLAVE_ALIAS, NO_EXISTE)
        if (aliasBorrar==aliasActual) {
            Comun.mostrarMensaje(this, "No puedes abandonar tu isla en esta pantalla")
            return
        }
        val dialogo = AlertDialog.Builder(this)
            .setMessage("¿Quiere borrar a '$aliasBorrar' de esta isla?")
            .setNegativeButton("Si", DialogInterface.OnClickListener() {
                    _: DialogInterface, _: Int ->
                subirBorrarAlias(aliasBorrar)  // -> refrescar la lista
            })
            .setPositiveButton("No", null)
            .setCancelable(false)
            .create()
        dialogo.show()
    }

    private fun subirBorrarAlias(aliasBorrar: String) {
        //pbEsperaEstado.visibility = View.VISIBLE
        habilitarEntrada(false)

        val preferencias = getSharedPreferences(PREFS_CREDENCIALES, Context.MODE_PRIVATE)
        val token = preferencias.getString(LLAVE_TOKEN, NO_EXISTE)

        val direccion = URL_BORRAR_MIEMBRO.replace(":id", "$idIsla") + "/$aliasBorrar"
        AndroidNetworking.delete(direccion)
            .addHeaders("Content-type","application/json; charset=utf-8")
            .addHeaders("Authorization", "Bearer $token")
            .build()
            .getAsOkHttpResponse(object: OkHttpResponseListener {
                override fun onResponse(response: Response?) {
                    //pbEsperaEstado.visibility = View.INVISIBLE
                    habilitarEntrada(true)
                    when (response?.code()) {
                        OK_BORRAR_MIEMBRO -> {
                            listaMiembros.remove(aliasBorrar)
                            crearAdaptadorListaMiembros()
                        }
                        ERROR_BORRAR_MIEMBRO_ACTIVO -> {
                            Comun.mostrarMensaje(
                                this@EstadoIslaActiv, "No se puede borrar porque hay un plan de seguridad activo para $aliasBorrar")
                        }
                        ERROR_BORRAR_MIEMBRO_NO_EXISTE -> {
                            Comun.mostrarMensaje(
                                this@EstadoIslaActiv, "No se puede borrar, m¡embro no existe: $aliasBorrar")
                        }
                    }
                }

                override fun onError(anError: ANError?) {
                    //pbEsperaEstado.visibility = View.INVISIBLE
                    habilitarEntrada(true)
                    when (anError?.errorCode) {
                        ERROR_INFO_INVALIDA -> {
                            Comun.mostrarMensaje(
                                this@EstadoIslaActiv, MENSAJE_CREDENCIALES_INVALIDAS, true, true
                            )
                        }
                        ERROR_NO_INTERNET -> {
                            Comun.mostrarMensaje(
                                this@EstadoIslaActiv, MENSAJE_NO_INTERNET
                            )
                        }
                        else -> {
                            Comun.mostrarMensaje(
                                this@EstadoIslaActiv, MENSAJE_ERROR_GENERAL
                            )
                        }
                    }
                }
            })
    }

    fun agregarMiembro(v: View) {
        val etAlias = EditText(this)
        val dialogo = AlertDialog.Builder(this)
            .setTitle("Nuevo integrante")
            .setMessage("Alias:\n")
            .setView(etAlias)
            .setPositiveButton("Aceptar", DialogInterface.OnClickListener() {
                    _: DialogInterface, _: Int ->
                val nuevo = etAlias.text.toString()
                subirAgregaAlias(nuevo)
            })
            .setNegativeButton("Cancelar", null)
            .setCancelable(false)
            .create()
        dialogo.show()
    }

    private fun subirAgregaAlias(aliasNuevo: String) {
        habilitarEntrada(false)
        //pbEsperaEstado.visibility = View.VISIBLE

        // Token
        val prefs = getSharedPreferences(PREFS_CREDENCIALES, Context.MODE_PRIVATE)
        val token = prefs.getString(LLAVE_TOKEN, "")

        val direccion = URL_AGREGAR_MIEMBRO.replace(":id", "$idIsla") + "/$aliasNuevo"

        AndroidNetworking.put(direccion)
            .addHeaders("Content-type","application/json; charset=utf-8")
            .addHeaders("Authorization", "Bearer $token")
            .build()
            .getAsJSONObject(object: JSONObjectRequestListener {
                override fun onResponse(response: JSONObject?) {
                    //pbEsperaEstado.visibility = View.INVISIBLE
                    habilitarEntrada(true)
                    Comun.mostrarMensaje(this@EstadoIslaActiv, "El alias se agregó con éxito")
                    descargarMiembros()  // Actualiza información
                }

                override fun onError(anError: ANError?) {
                    //pbEsperaEstado.visibility = View.INVISIBLE
                    habilitarEntrada(true)
                    when (anError?.errorCode) {
                        ERROR_MIEMBRO_NO_EXISTE -> {
                            Comun.mostrarMensaje(this@EstadoIslaActiv, "El alias '$aliasNuevo' no existe")
                        }
                        ERROR_MIEMBRO_YA_EXISTE -> {
                            Comun.mostrarMensaje(this@EstadoIslaActiv, "El alias '$aliasNuevo' ya existe en la isla")
                        }
                        ERROR_INFO_INVALIDA -> {
                            Comun.mostrarMensaje(
                                this@EstadoIslaActiv, MENSAJE_CREDENCIALES_INVALIDAS, true, true)
                        }
                        ERROR_NO_INTERNET -> {
                            Comun.mostrarMensaje(
                                this@EstadoIslaActiv, MENSAJE_NO_INTERNET, true)
                        }
                        else -> {
                            Comun.mostrarMensaje(
                                this@EstadoIslaActiv, MENSAJE_ERROR_GENERAL, true)
                        }
                    }
                }
            })
    }

    fun consultarPlan(v: View) {
        val intDocumentoPlan = Intent(this, DocumentoPlanActiv::class.java)
        // Pasar idIsla, dIsla y nombreIsla
        intDocumentoPlan.putExtra(LLAVE_ID_ISLA_SELECCIONADA, idIsla)
        intDocumentoPlan.putExtra(LLAVE_NOMBRE_ISLA, nombreIsla)
        startActivity(intDocumentoPlan)
    }

    // Cambiar plan de seguridad
    fun cambiarPlanSeguridad(v: View) {
        if (listaMiembros.size<=0) {
            Comun.mostrarMensaje(this, "No hay miembros suficientes en la isla")
            return
        }
        // Menu, selecciona alias....estable
        val arrAlias = mutableListOf<String>()

        arrAlias.addAll(listaMiembros)
        arrAlias.add("Regresar a Estable")

        var indiceSeleccionado = 0
        val builder = AlertDialog.Builder(this)
            .setTitle("Selecciona a la persona ausente")
            .setSingleChoiceItems(arrAlias.toTypedArray(), 0
            ) { _, which ->
                indiceSeleccionado = which
            }
            // Set the action buttons
            .setPositiveButton("Aceptar") { _, _ ->
                var aliasSeleccionado = arrAlias[indiceSeleccionado]
                if (aliasSeleccionado=="Regresar a Estable") {
                    aliasSeleccionado = "estable"
                }
                subirActivarPlanSeguridad(aliasSeleccionado)
            }
            .setNegativeButton("Cancelar", null)

        builder.create()
        builder.show()
    }

    private fun subirActivarPlanSeguridad(alias: String) {
        //pbEsperaEstado.visibility = View.VISIBLE
        habilitarEntrada(false
        )
        // Token
        val prefs = getSharedPreferences(PREFS_CREDENCIALES, Context.MODE_PRIVATE)
        val token = prefs.getString(LLAVE_TOKEN, "")

        val direccion = URL_ACTIVA_PLAN_SEGURIDAD.replace(":id", "$idIsla")

        var jsonBody = JSONObject(
            """
            {
                "missing_person": "$alias"
            }
            """.trimIndent()
        )

        if (alias == "estable") {
            jsonBody = JSONObject("""
            {
            "stable": true
            }
            """.trimIndent())
        }

        AndroidNetworking.put(direccion)
            .addHeaders("Content-type","application/json; charset=utf-8")
            .addHeaders("Authorization", "Bearer $token")
            .addJSONObjectBody(jsonBody)
            .build()
            .getAsJSONObject(object: JSONObjectRequestListener {
                override fun onResponse(response: JSONObject?) {
                    //pbEsperaEstado.visibility = View.INVISIBLE
                    habilitarEntrada(true)
                    actualizarDatosPlan(response!!, alias)
                }

                override fun onError(anError: ANError?) {
                    //pbEsperaEstado.visibility = View.INVISIBLE
                    habilitarEntrada(true)
                    when (anError?.errorCode) {
                        ERROR_INFO_INVALIDA -> {
                            Comun.mostrarMensaje(
                                this@EstadoIslaActiv, MENSAJE_CREDENCIALES_INVALIDAS, true, true)
                        }
                        ERROR_NO_INTERNET -> {
                            Comun.mostrarMensaje(
                                this@EstadoIslaActiv, MENSAJE_NO_INTERNET, true)
                        }
                        else -> {
                            Comun.mostrarMensaje(
                                this@EstadoIslaActiv, MENSAJE_ERROR_GENERAL, true)
                        }
                    }
                }
            })
    }

    private fun actualizarDatosPlan(response: JSONObject, alias: String) {
        //println("Actualiza plan $response")

        var ndPlanSeguridad: JSONObject? = null
        var nestaEstable: Boolean? = null
        try {
            ndPlanSeguridad = response.getJSONObject("active_security_plan")
        } catch (e: JSONException) {
        }

        try {
            nestaEstable = response.getBoolean("stable")
        } catch (e: JSONException) {
        }

        if (nestaEstable!=null) {
            val estable = nestaEstable!!
            if (estable) {
                islaEstable = true
                tvEstado.text = "Estado: Estable"
                val modelo = response.getString("governance_model")
                tvModelo.text = "Modelo de gobernanza: ${mapaModelo[modelo]}"
                if (modelo=="centralized") {
                    val usuarioDecisiones = response.getString("centralized_user")
                    tvModelo.append(" ($usuarioDecisiones)")
                }
            }
            Comun.mostrarMensaje(this, "Se ha cambiado el estado de la isla")
            actualizarDIslaPreferencias(response)
            return
        }

        val dPlanSeguridad = ndPlanSeguridad!!

        if (dPlanSeguridad!=null) {
            islaEstable = false
            if (dPlanSeguridad!!.has("missing_person")) {
                val plan = dPlanSeguridad!!.getString("missing_person")
                tvEstado.text = "Persona desaparecida: $plan"
            }
            val dModeloGob = dPlanSeguridad.getJSONObject("governance_details")
            val modeloGob = dModeloGob.getString("winner")
            tvModelo.text = "Modelo de gobernanza: ${mapaModelo[modeloGob]}"
            if (modeloGob=="centralized") {
                // Preguntar quien toma las decisiones
                designarCentralizado(alias)
            }  else if (modeloGob=="weighted") {
                // Pregunta los pesos de cada uno (1-5)
                capturarPesos()
            }
        }
    }

    private fun actualizarDIslaPreferencias(response: JSONObject) {
        //println("Actualizando prefs: $response")
        if (response.has("group")) {
            dIsla = response.getJSONObject("group")
        } else {
            dIsla = response
        }
        //println("Nueva dIsla")
        val idISla = dIsla.getInt("id")
        val prefs = getSharedPreferences(PREFS_CREDENCIALES, Context.MODE_PRIVATE)
        prefs.edit().apply {
            putInt(LLAVE_ID_ISLA_SELECCIONADA, idISla)
            putString(LLAVE_JSON_ISLA, dIsla.toString())
            commit()
        }
    }

    private fun capturarPesos() {
        val intPesos = Intent(this, CapturaPesosActiv::class.java)
        // enviar arreglo de alias
        intPesos.putExtra(LLAVE_ARR_ALIAS, listaMiembros.toTypedArray())
        startActivityForResult(intPesos, CODIGO_CAPTURA_PESOS)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode==CODIGO_CAPTURA_PESOS) {
            if (resultCode== Activity.RESULT_OK) {
                // Enviar los pesos al servidor
                val arrPesos = data?.getIntArrayExtra(LLAVE_ARR_PESOS) ?: return
                subirPesos(arrPesos)
            }
        }
    }

    private fun subirPesos(arrPesos: IntArray) {
        //pbEsperaEstado.visibility = View.VISIBLE
        habilitarEntrada(false)

        // Token
        val prefs = getSharedPreferences(PREFS_CREDENCIALES, Context.MODE_PRIVATE)
        val token = prefs.getString(LLAVE_TOKEN, "")

        val direccion = URL_ASIGNAR_PESOS_PONDERADO.replace(":id", "$idIsla")

        // Craer body
        var strBody = ""
        for (i in 0 until listaMiembros.size) {
            val alias = listaMiembros[i]
            val peso = arrPesos[i]
            strBody += "'$alias' : $peso"
            if (i<listaMiembros.size-1) {
                strBody += ",\n"
            }
        }

        var jsonBody = JSONObject(
            """
            { 'weights':
                {
                $strBody
                }
            }
            """.trimIndent()
        )

        AndroidNetworking.put(direccion)
            .addHeaders("Content-type","application/json; charset=utf-8")
            .addHeaders("Authorization", "Bearer $token")
            .addJSONObjectBody(jsonBody)
            .build()
            .getAsJSONObject(object: JSONObjectRequestListener {
                override fun onResponse(response: JSONObject?) {
                    //pbEsperaEstado.visibility = View.INVISIBLE
                    habilitarEntrada(true)
                    tvModelo.text = "Modelo de gobernanza: Ponderado"
                    Comun.mostrarMensaje(this@EstadoIslaActiv, "Se asignaron correctamente los pesos")
                    actualizarDIslaPreferencias(response!!)
                }

                override fun onError(anError: ANError?) {
                    //pbEsperaEstado.visibility = View.INVISIBLE
                    habilitarEntrada(true)
                    when (anError?.errorCode) {
                        ERROR_INFO_INVALIDA -> {
                            Comun.mostrarMensaje(
                                this@EstadoIslaActiv, MENSAJE_CREDENCIALES_INVALIDAS, true, true)
                        }
                        ERROR_NO_INTERNET -> {
                            Comun.mostrarMensaje(
                                this@EstadoIslaActiv, MENSAJE_NO_INTERNET, true)
                        }
                        else -> {
                            Comun.mostrarMensaje(
                                this@EstadoIslaActiv, MENSAJE_ERROR_GENERAL, true)
                        }
                    }
                }
            })
    }

    private fun designarCentralizado(aliasAusente: String) {
        var arrAlias = listaMiembros.toMutableList()
        arrAlias.remove(aliasAusente)    // El ausente no puede tomar decisiones
        if (arrAlias.isNotEmpty()) {
            // Menu, selecciona alias.... que toma las decisiones
            var indiceSeleccionado = 0
            val builder = AlertDialog.Builder(this)

                .setTitle("Selecciona a la persona que toma las decisiones")
                .setSingleChoiceItems(arrAlias.toTypedArray(), 0) { _, which ->
                    indiceSeleccionado = which
                }
                .setPositiveButton("Aceptar")
                { _, _ ->
                    var aliasSeleccionado = arrAlias[indiceSeleccionado]
                    subirMiembroCentralizado(aliasSeleccionado)
                }
                .setNegativeButton("Cancelar", null)

            builder.create()
            builder.show()
        }
    }

    private fun subirMiembroCentralizado(alias: String) {
        //pbEsperaEstado.visibility = View.VISIBLE
        habilitarEntrada(false)

        // Token
        val prefs = getSharedPreferences(PREFS_CREDENCIALES, Context.MODE_PRIVATE)
        val token = prefs.getString(LLAVE_TOKEN, "")

        val direccion = URL_ASIGNAR_CENTRALIZADO.replace(":id", "$idIsla")

        var jsonBody = JSONObject(
            """
            {
            "user": "$alias"
            }
            """.trimIndent()
        )

        AndroidNetworking.put(direccion)
            .addHeaders("Content-type","application/json; charset=utf-8")
            .addHeaders("Authorization", "Bearer $token")
            .addJSONObjectBody(jsonBody)
            .build()
            .getAsJSONObject(object: JSONObjectRequestListener {
                override fun onResponse(response: JSONObject?) {
                    //pbEsperaEstado.visibility = View.INVISIBLE
                    habilitarEntrada(true)
                    Comun.mostrarMensaje(this@EstadoIslaActiv, "Se asignó a '$alias' como la persona que toma decisiones")
                    tvModelo.text = "Modelo de gobernanza: Centralizado ($alias)"
                    actualizarDIslaPreferencias(response!!)
                }

                override fun onError(anError: ANError?) {
                    //pbEsperaEstado.visibility = View.INVISIBLE
                    habilitarEntrada(true)
                    when (anError?.errorCode) {
                        ERROR_INFO_INVALIDA -> {
                            Comun.mostrarMensaje(
                                this@EstadoIslaActiv, MENSAJE_CREDENCIALES_INVALIDAS, true, true)
                        }
                        ERROR_NO_INTERNET -> {
                            Comun.mostrarMensaje(
                                this@EstadoIslaActiv, MENSAJE_NO_INTERNET, true)
                        }
                        else -> {
                            Comun.mostrarMensaje(
                                this@EstadoIslaActiv, MENSAJE_ERROR_GENERAL, true)
                        }
                    }
                }
            })
    }

    fun cambiarModeloGobernanza(v: View) {
        if (!islaEstable) {
            Comun.mostrarMensaje(this, "Hay un plan de seguridad activo.\nPara modificarlo, puedes redefinir roles y modelo de gobernanza.")
            return
        }

        val arrModelos = arrayOf("Democrático", "Centralizado", "Ponderado")

        var indiceSeleccionado = 0
        val builder = AlertDialog.Builder(this)
            .setTitle("Selecciona el nuevo modelo")
            .setSingleChoiceItems(arrModelos, 0
            ) { _, which ->
                indiceSeleccionado = which
            }
            // Set the action buttons
            .setPositiveButton("Aceptar") { _, _ ->
                var modeloSeleccionado = arrModelos[indiceSeleccionado]
                subirCambioModelo(modeloSeleccionado)
            }
            .setNegativeButton("Cancelar", null)

        builder.create()
        builder.show()
    }

    private fun subirCambioModelo(nuevoModelo: String) {
        //pbEsperaEstado.visibility = View.VISIBLE
        habilitarEntrada(false)

        // Token
        val prefs = getSharedPreferences(PREFS_CREDENCIALES, Context.MODE_PRIVATE)
        val token = prefs.getString(LLAVE_TOKEN, "")

        val direccion = URL_CAMBIA_MG_ESTABLE.replace(":id", "$idIsla")

        var jsonBody = JSONObject(
            """
            {
            "governance_model": "${modelosIngles[nuevoModelo]}"
            }
            """.trimIndent()
        )

        AndroidNetworking.put(direccion)
            .addHeaders("Content-type","application/json; charset=utf-8")
            .addHeaders("Authorization", "Bearer $token")
            .addJSONObjectBody(jsonBody)
            .build()
            .getAsJSONObject(object: JSONObjectRequestListener {
                override fun onResponse(response: JSONObject?) {
                    //pbEsperaEstado.visibility = View.INVISIBLE
                    habilitarEntrada(true)
                    ajustarParametrosModelo(nuevoModelo)
                }

                override fun onError(anError: ANError?) {
                    //pbEsperaEstado.visibility = View.INVISIBLE
                    habilitarEntrada(true)
                    when (anError?.errorCode) {
                        ERROR_INFO_INVALIDA -> {
                            Comun.mostrarMensaje(
                                this@EstadoIslaActiv, MENSAJE_CREDENCIALES_INVALIDAS, true, true)
                        }
                        ERROR_NO_INTERNET -> {
                            Comun.mostrarMensaje(
                                this@EstadoIslaActiv, MENSAJE_NO_INTERNET, true)
                        }
                        else -> {
                            Comun.mostrarMensaje(
                                this@EstadoIslaActiv, MENSAJE_ERROR_GENERAL, true)
                        }
                    }
                }
            })
    }

    private fun ajustarParametrosModelo(nuevoModelo: String) {

        when (nuevoModelo) {
            "Centralizado" -> {
                tvModelo.text = "Modelo de gobernanza: Centralizado"
                var arrAlias = listaMiembros.toMutableList()
                // Menu, selecciona alias.... que toma las decisiones
                var indiceSeleccionado = 0
                val builder = AlertDialog.Builder(this)
                    .setTitle("Selecciona a la persona que toma las decisiones")
                    .setSingleChoiceItems(
                        arrAlias.toTypedArray(), 0
                    ) { _, which ->
                        indiceSeleccionado = which
                    }
                    .setPositiveButton("Aceptar")
                    { _, _ ->
                        var aliasSeleccionado = arrAlias[indiceSeleccionado]
                        subirMiembroCentralizado(aliasSeleccionado)
                    }
                    .setNegativeButton("Cancelar", null)

                builder.create()
                builder.show()
            }
            "Ponderado" -> {
                tvModelo.text = "Modelo de gobernanza: Ponderado"
                capturarPesos()
            }
            "Democrático" -> {
                tvModelo.text = "Modelo de gobernanza: Democrático"
            }
        }
    }

    private var entradaHabilitada = true

    private fun habilitarEntrada(habilitada: Boolean) {
        pbEsperaEstado.visibility = if (habilitada) View.INVISIBLE else View.VISIBLE
        entradaHabilitada = habilitada
    }

    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        return if (entradaHabilitada) super.dispatchTouchEvent(ev) else true
    }
}
