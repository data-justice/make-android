package mx.rmr.make.plan

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.FileProvider
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONArrayRequestListener
import io.github.lucasfsc.html2pdf.Html2Pdf
import kotlinx.android.synthetic.main.activity_documento_plan.*
import mx.rmr.make.*
import mx.rmr.make.util.Comun
import org.json.JSONArray
import org.json.JSONException
import java.io.File


class DocumentoPlanActiv : AppCompatActivity(), Html2Pdf.OnCompleteConversion {
    private var idIsla: Int = 0
    private lateinit var arrPlanes: JSONArray
    private lateinit var nombreIsla: String

    private var mapaModelo = mapOf("democratic" to "Democrático",
        "centralized" to "Centralizado", "weighted" to "Ponderado")

    private var textoHtml = ""
    private lateinit var pdfFile: File

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_documento_plan)

        idIsla = intent.getIntExtra(LLAVE_ID_ISLA_SELECCIONADA, 0)
        nombreIsla = intent.getStringExtra(LLAVE_NOMBRE_ISLA)
        descargarPlanSeguridad()
    }

    private fun descargarPlanSeguridad() {
        habilitarEntrada(false)

        val preferencias = getSharedPreferences(PREFS_CREDENCIALES, Context.MODE_PRIVATE)
        val token = preferencias.getString(LLAVE_TOKEN, NO_EXISTE)

        val direccion = URL_LISTA_PLANES_DE_SEGURIDAD.replace(":id", "$idIsla")

        AndroidNetworking.get(direccion)
            .addHeaders("Content-type","application/json; charset=utf-8")
            .addHeaders("Authorization", "Bearer $token")
            .build()

            .getAsJSONArray(object: JSONArrayRequestListener {
                override fun onResponse(response: JSONArray?) {
                    habilitarEntrada(true)
                    if (response!=null) {
                        arrPlanes = response
                        generarHTML()
                    }
                }

                override fun onError(anError: ANError?) {
                    habilitarEntrada(true)
                    if (anError != null) {
                        when (anError.errorCode) {
                            ERROR_INFO_INVALIDA -> {
                                Comun.mostrarMensaje(
                                    this@DocumentoPlanActiv, MENSAJE_CREDENCIALES_INVALIDAS, true, true)
                            }
                            ERROR_NO_INTERNET -> {
                                Comun.mostrarMensaje(
                                    this@DocumentoPlanActiv, MENSAJE_NO_INTERNET
                                )
                            }
                            else -> {
                                Comun.mostrarMensaje(
                                    this@DocumentoPlanActiv, MENSAJE_ERROR_GENERAL
                                )
                            }
                        }
                    }
                }
            })
    }

    private fun generarHTML() {
        // Genera el HTML de los planes de seguridad
        val strHTML = crearHTML()
        textoHtml = strHTML
        webPlan.loadDataWithBaseURL("file:///android_asset/", textoHtml, "text/html", "UTF-8", null)
    }

    private fun crearHTML(): String {
        var texto = """
        <!DOCTYPE html>
        <html>
        <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Plan de seguridad</title>
        </head>
        <body style="font-size:12px; font-family:'Arial'">
        <img src="file:///android_asset/MAKE_512.png" width="128" alt="MaKE"/>
        <h2>Plan de seguridad para la isla <em>$nombreIsla</em></h2>
        <br>
        """.trimIndent()


        for (i in 0 until arrPlanes.length()) {
            val dPlan = arrPlanes.getJSONObject(i)

            var nPersonaAusente: String? = null
            try {
                nPersonaAusente = dPlan.getString("missing_person")
            }catch (e: JSONException) {
            }
            if (nPersonaAusente==null || nPersonaAusente=="null") {
                continue
            }

            val personaAusente = nPersonaAusente!!
            texto += "<h3>Persona ausente: $personaAusente</h3>"

            val dMiembro = dPlan.getJSONObject("members_roles")

            // Roles de cada miembro
            for (miembro in dMiembro.keys()) {
                val arrRoles = dMiembro.getJSONArray(miembro)
                texto += "<h4>El miembro: $miembro</h4>"
                texto += "Tiene los siguientes roles:"
                texto += "<ul>"
                for (j in 0 until arrRoles.length()) {
                    val dRol = arrRoles.getJSONObject(j)
                    val rol = dRol.getString("role")
                    texto += "<li> $rol</li>"
                }
                texto += "</ul>"
            }
            // Gobernanza
            val dGobernanza = dPlan.getJSONObject("governance_details")
            texto += "<h3>Modelo de gobernanza: "
            if (dGobernanza.has("winner")) {
                val modeloGobernanza = dGobernanza.getString("winner")
                texto += "${mapaModelo[modeloGobernanza]}"
            } else {
                texto += "Desconocido"
            }
            texto += "</h3><hr>"
        }

        texto += "</body></html>"

        return texto
    }

    fun compartirDocumento(v: View) {
        pdfFile = File.createTempFile("plan_", ".pdf")

        val converter = Html2Pdf.Companion.Builder()
            .context(this)
            .html(textoHtml)
            .file(pdfFile)
            .build()

        converter.convertToPdf(this)
    }

    override fun onFailed() {
        Comun.mostrarMensaje(this, "No puedes compartir el plan de seguridad")
    }

    override fun onSuccess() {
        var pdfUri = FileProvider.getUriForFile(this, packageName+".provider", pdfFile)
        grantUriPermission(packageName, pdfUri, Intent.FLAG_GRANT_READ_URI_PERMISSION)

        val intCompartir = Intent()
        intCompartir.action = Intent.ACTION_SEND
        intCompartir.type = "application/pdf"
        intCompartir.flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
        intCompartir.putExtra(Intent.EXTRA_STREAM, pdfUri)
        intCompartir.putExtra(Intent.EXTRA_SUBJECT, "Plan de seguridad")
        intCompartir.putExtra(Intent.EXTRA_TEXT, "Este es el plan de seguridad para la isla '$nombreIsla'")
        val chooser = Intent.createChooser(intCompartir, "Compartir plan de seguridad")

        val resInfoList = this.packageManager
            .queryIntentActivities(chooser, PackageManager.MATCH_DEFAULT_ONLY)
        for (resolveInfo in resInfoList) {
            val packageName = resolveInfo.activityInfo.packageName
            grantUriPermission(
                packageName,
                pdfUri,
                Intent.FLAG_GRANT_READ_URI_PERMISSION
            )
        }

        startActivity(chooser)
    }


    fun mostrarPlanGrafico(v: View) {
        // Generar menú para preguntar el plan que quiere ver el usuario
        var dIndices = mutableMapOf<String, Int>()
        var indice = 0
        if (arrPlanes.length()>0) {
            for (i in 0 until arrPlanes.length()) {
                val dPlan = arrPlanes.getJSONObject(i)

                var personaAusente = "null"
                if (dPlan.has("missing_person")) {
                    personaAusente = dPlan.getString("missing_person")
                }
                if (personaAusente != "null") {
                    dIndices[personaAusente] = indice
                }
                indice += 1
            }

            var arrAlias = dIndices.keys.toTypedArray()
            // Menu, selecciona alias.... que toma las decisiones
            var indiceSeleccionado = 0
            val builder = AlertDialog.Builder(this)
                .setTitle("Selecciona plan de seguridad")
                .setSingleChoiceItems(
                    arrAlias, 0
                ) { _, which ->
                    indiceSeleccionado = which
                }
                .setPositiveButton("Aceptar")
                { _, _ ->
                    val aliasSeleccionado = arrAlias[indiceSeleccionado]
                    val indiceAlias = dIndices[aliasSeleccionado]!!
                    val plan = arrPlanes.getJSONObject(indiceAlias)
                    //println("plan = $plan")
                    val intPlanGrafico = Intent(this, PlanGraficoActiv::class.java)
                    intPlanGrafico.putExtra(LLAVE_JSON_ISLA, plan.toString())
                    startActivity(intPlanGrafico)
                }
                .setNegativeButton("Cancelar", null)

            builder.create()
            builder.show()

        } else {
            Comun.mostrarMensaje(this,"No hay planes de seguridad")
        }
    }

    private var entradaHabilitada = true

    private fun habilitarEntrada(habilitada: Boolean) {
        pbDocPlan.visibility = if (habilitada) View.INVISIBLE else View.VISIBLE
        entradaHabilitada = habilitada
    }

    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        return if (entradaHabilitada) super.dispatchTouchEvent(ev) else true
    }
}
