package mx.rmr.make.util

import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AlertDialog
import mx.rmr.make.login.LoginActiv

class Comun
{
    companion object {
        fun esconderTeclado(actvidad: Activity) {
            val inputMethodManager = actvidad.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(actvidad.currentFocus?.windowToken, 0)
        }

        fun mostrarMensaje(actividad: Activity, mensaje: String,
                           cerrarVentana: Boolean = false, hacerLogout: Boolean= false) {
                            val dialogo = AlertDialog.Builder(actividad)
                                .setMessage(mensaje)
                                .setPositiveButton("Aceptar", DialogInterface.OnClickListener() {
                                        _: DialogInterface, _: Int ->
                                        if (hacerLogout) {
                                            val intPrincipal = Intent(actividad, LoginActiv::class.java)
                                            actividad.startActivity(intPrincipal)
                                            actividad.finishAffinity()
                                        } else if(cerrarVentana) {
                                            actividad.finish()
                                        }
                                })
                                .setCancelable(false)
                                .create()
                            dialogo.show()
        }
    }
}