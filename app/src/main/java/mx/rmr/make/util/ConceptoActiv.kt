package mx.rmr.make.util

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import android.view.View
import kotlinx.android.synthetic.main.activity_concepto.*
import mx.rmr.make.*

class ConceptoActiv : AppCompatActivity()
{
    private var intSiguiente: Intent? = null
    private var LLAVE_EXPLICA = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_concepto)

        val concepto = intent.getStringExtra(LLAVE_CONCEPTO_EXPLICA)
        tvTituloConcepto.text = dConceptos[concepto]?.first ?: "Concepto"
        tvConcepto.text = dConceptos[concepto]?.second ?: "Descripción"
        tvConcepto.movementMethod = ScrollingMovementMethod()

        if (intent.extras?.get(LLAVE_SIGUIENTE_ACTIV) != null) {
            intSiguiente = intent.extras?.get(LLAVE_SIGUIENTE_ACTIV) as Intent
        }
        LLAVE_EXPLICA = concepto
    }

    fun cancelar(v: View) {
        finish()
    }

    fun aceptar(v: View) {
        if (intSiguiente != null) {
            startActivity(intSiguiente)
        }
        finish()
    }

    fun aceptarSiempre(v: View) {
        val prefs = getSharedPreferences(PREFS_CONCEPTOS, Context.MODE_PRIVATE)
        prefs.edit().apply {
            putBoolean(LLAVE_EXPLICA, true)
            commit()
        }
        aceptar(v)
    }

    //{ "Isla": ("Titulo", "Descripcion") }
    private val dConceptos = mapOf<String, Pair<String,String>>(
        LLAVE_CONCEPTO_ISLA to Pair("Isla de confianza", """Descripción de una Isla de confianza.
            
            Círculo de personas, familiares, amigos o colegas, que pueden apoyar o accionar de manera inmediata ante una crisis, como en un supuesto caso de secuestro.
            """.trimIndent()),
        LLAVE_CONCEPTO_ROL to Pair("Roles", """
            Definir roles de manera preventiva ayuda a tomar decisiones objetivamente y ganar tiempo en caso de un secuestro o situación de crisis.
                        
            Algunos roles necesarios son: negociación, acercamiento con autoridades, compra de alimentos y/o medicinas, cuidado de menores, entre otros. Estos roles dependerán de la dinámica que cada Isla de confianza tenga, pudiendo surgir nuevos roles.
        """.trimIndent()),
        LLAVE_CONCEPTO_MODELO_GOB to Pair("Modelo de Gobernanza","""
            ¿Qué son?
            
            Son formatos para la toma de decisiones. Cada isla de confianza decidirá la manera en que tomará decisiones. Existen 3 tipos:
            
            1. Democrático: Cada miembro de la isla de confianza cuenta con un solo voto.
            
            2. Ponderado: El número de votos de cada miembro dependerá de sus habilidades y/o grados de aportación ante una crisis, como un supuesto caso de secuestro.
            
            3. Centralizado: Una sola persona tomará las decisiones por toda la isla de confianza.
        """.trimIndent())
    )
}
