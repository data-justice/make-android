package mx.rmr.make.encuestas

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import kotlinx.android.synthetic.main.activity_crea_encuesta.*
import mx.rmr.make.*
import mx.rmr.make.util.Comun
import org.json.JSONArray
import org.json.JSONObject

class CreaEncuestaActiv : AppCompatActivity()
{
    private var idIsla = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_crea_encuesta)

        val prefs = getSharedPreferences(PREFS_CREDENCIALES, Context.MODE_PRIVATE)
        idIsla = prefs.getInt(LLAVE_ID_ISLA_SELECCIONADA, 0)
    }

    fun crearEncuesta(v: View) {
        val titulo = tvTituloCreaEncuesta.text.toString()
        //val descripcion = URLEncoder.encode(tvDescripcionCreaEncuesta.text.toString(), "UTF-8")
        val descripcion = tvDescripcionCreaEncuesta.text.toString()
        val opcion1 = tvOpcion_1.text.toString()
        val opcion2 = tvOpcion_2.text.toString()
        val opcion3 = tvOpcion_3.text.toString()
        val opcion4 = tvOpcion_4.text.toString()

        val grupo = idIsla

        var listaRespuesta = mutableListOf<Map<String,String>>()

        if (opcion1.isNotBlank()) {
            listaRespuesta.add(mapOf("title" to opcion1))
        }
        if (opcion2.isNotBlank()) {
            listaRespuesta.add(mapOf("title" to opcion2))
        }
        if (opcion3.isNotBlank()) {
            listaRespuesta.add(mapOf("title" to opcion3))
        }
        if (opcion4.isNotBlank()) {
            listaRespuesta.add(mapOf("title" to opcion4))
        }

        val listaOpciones = JSONArray(listaRespuesta)

        if (listaRespuesta.size<2 || titulo.isBlank() || descripcion.isBlank()) {
            if (listaRespuesta.size < 2) {
                if (opcion1.isBlank()) {
                    tvOpcion_1.error = "Obligatorio"
                }
                if (opcion2.isBlank()) {
                    tvOpcion_2.error = "Obligatorio"
                }
            }
            if (titulo.isEmpty()) {
                tvTituloCreaEncuesta.error = "Captura el título"
            }
            if (descripcion.isEmpty()) {
                tvDescripcionCreaEncuesta.error = "Captura la descripción"
            }
            return
        }

        // pbEsperaCreaEncuesta.visibility = View.VISIBLE
        habilitarEntrada(false)

        // Token
        val prefs = getSharedPreferences(PREFS_CREDENCIALES, Context.MODE_PRIVATE)
        val token = prefs.getString(LLAVE_TOKEN, "")

        val direccion = URL_CREA_ENCUESTA

        var jsonBody = JSONObject(
            """
            {
            "group": $grupo,
            "title": "$titulo",
            "description": "$descripcion",
            "choices": $listaOpciones
            }
            """.trimIndent()
        )

        AndroidNetworking.post(direccion)
            .addHeaders("Content-type","application/json; charset=utf-8")
            .addHeaders("Authorization", "Bearer $token")
            .addJSONObjectBody(jsonBody)
            .build()
            .getAsJSONObject(object: JSONObjectRequestListener {
                override fun onResponse(response: JSONObject?) {
                    //pbEsperaCreaEncuesta.visibility = View.INVISIBLE
                    habilitarEntrada(true)
                    Comun.mostrarMensaje(this@CreaEncuestaActiv, "Se creó la encuesta con éxito", true)
                }

                override fun onError(anError: ANError?) {
                    //pbEsperaCreaEncuesta.visibility = View.INVISIBLE
                    habilitarEntrada(true)
                    when (anError?.errorCode) {
                        ERROR_INFO_INVALIDA -> {
                            Comun.mostrarMensaje(
                                this@CreaEncuestaActiv, MENSAJE_CREDENCIALES_INVALIDAS, true, true)
                        }
                        ERROR_NO_INTERNET -> {
                            Comun.mostrarMensaje(
                                this@CreaEncuestaActiv, MENSAJE_NO_INTERNET, true)
                        }
                        else -> {
                            Comun.mostrarMensaje(
                                this@CreaEncuestaActiv, MENSAJE_ERROR_GENERAL, true)
                        }
                    }
                }
            })
    }

    private var entradaHabilitada = true

    private fun habilitarEntrada(habilitada: Boolean) {
        pbEsperaCreaEncuesta.visibility = if (habilitada) View.INVISIBLE else View.VISIBLE
        entradaHabilitada = habilitada
    }

    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        return if (entradaHabilitada) super.dispatchTouchEvent(ev) else true
    }
}
