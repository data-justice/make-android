package mx.rmr.make.encuestas

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.rv_renglon_encuesta.view.*
import mx.rmr.make.R
import mx.rmr.make.eventos.ListenerConsultaEvento

class AdaptadorEncuestas (private val contexto: Context, var arrEncuestas: Array<Encuesta>):
RecyclerView.Adapter<AdaptadorEncuestas.RenglonEncuesta>()
{
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RenglonEncuesta {
        val vista = LayoutInflater.from(parent.context)
            .inflate(R.layout.rv_renglon_encuesta, parent, false)
        return RenglonEncuesta(vista)
    }

    override fun getItemCount(): Int {
        return arrEncuestas.size
    }

    override fun onBindViewHolder(holder: RenglonEncuesta, position: Int) {
        val encuesta = arrEncuestas[position]
        holder.vistaRenglon.tvRenglonEncuesta.text = encuesta.titulo
        if (encuesta.contestada) {
            holder.vistaRenglon.tvRenglonEncuesta
                .setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.check,0)
        } else {
            holder.vistaRenglon.tvRenglonEncuesta
                .setCompoundDrawablesWithIntrinsicBounds(0,0,0,0)
        }

        // Listener
        holder.vistaRenglon.setOnClickListener {
            val listener = contexto as ListenerConsultaEvento
            listener.btnClicked(position)
        }
    }

    inner class RenglonEncuesta(var vistaRenglon: View): RecyclerView.ViewHolder(vistaRenglon)
    {
    }
}