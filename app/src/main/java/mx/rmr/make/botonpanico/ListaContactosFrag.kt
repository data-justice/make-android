package mx.rmr.make.botonpanico

import android.content.Context
import android.view.View
import android.widget.ListView
import android.widget.SimpleAdapter
import androidx.fragment.app.ListFragment


class ListaContactosFrag : ListFragment()
{
    private val arrDatos = mutableListOf<Map<String,String>>()
    var contexto: Context? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        this.contexto = context
    }

    fun cambiarAdaptador(arrContactos: MutableList<Contacto>?) {
        arrDatos.clear()

        if (arrContactos!=null) {
            for (contacto in arrContactos) {
                val entrada = HashMap<String, String>(2);
                entrada["title"] = contacto.nombre
                entrada["subtitle"] = contacto.telefono
                arrDatos.add(entrada)
            }

            val adapter = SimpleAdapter(
                contexto,
                arrDatos,
                android.R.layout.simple_list_item_2,
                arrayOf("title", "subtitle"),
                intArrayOf(
                    android.R.id.text1,
                    android.R.id.text2
                )
            )

            listAdapter = adapter

            // Click largo para borrar
            listView.setOnItemLongClickListener { _, _, position, _ ->
                val listener = contexto as ListenerFragmentoLista
                listener.longClickOnIndex(position)
                true
            }
        }
    }
}
