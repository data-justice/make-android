package mx.rmr.make.botonpanico

class Contacto(val nombre: String, val telefono: String)
{
    override fun toString(): String {
        return "$nombre,$telefono"
    }
}
