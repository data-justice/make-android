package mx.rmr.make.islas

import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.ListFragment
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import kotlinx.android.synthetic.main.activity_crea_isla.*
import mx.rmr.make.*
import mx.rmr.make.botonpanico.ListenerFragmentoLista
import mx.rmr.make.util.Comun
import org.json.JSONObject


class CreaIslaActiv : AppCompatActivity(), ListenerFragmentoLista
{
    lateinit var arrAlias: MutableList<String>
    lateinit var fragAlias: ListaCadenasFrag

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_crea_isla)

        arrAlias = mutableListOf()
        fragAlias = supportFragmentManager.findFragmentById(R.id.fragListaAlias) as ListaCadenasFrag
    }

    override fun onResume() {
        super.onResume()

        fragAlias.cambiarAdaptador(arrAlias)
    }

    fun enviarCreaIsla(v: View) {
        if (etNombreIslaNueva.text!!.isEmpty()) {
            etNombreIslaNueva.error = "Necesita un valor"
            return
        }
        if (arrAlias.isEmpty()) {
            Comun.mostrarMensaje(this, "No puedes crear una isla vacía")
            return
        }

        //pbEsperaCreaIsla.visibility = View.VISIBLE
        habilitarEntrada(false)

        val nombreIsla = etNombreIslaNueva.text.toString()

        val jsonBody = JSONObject("""{
            'name': '$nombreIsla',
            'members': $arrAlias
            }
        """.trimIndent())

        // Token
        val prefs = getSharedPreferences(PREFS_CREDENCIALES, Context.MODE_PRIVATE)
        val token = prefs.getString(LLAVE_TOKEN, "")

        val urlDireccion = URL_CREA_ISLA

        AndroidNetworking.post(urlDireccion)
            .addHeaders("Content-type","application/json; charset=utf-8")
            .addHeaders("Authorization", "Bearer $token")
            .addJSONObjectBody(jsonBody)
            .build()
            .getAsJSONObject(object: JSONObjectRequestListener {
                override fun onResponse(response: JSONObject?) {
                    //pbEsperaCreaIsla.visibility = View.INVISIBLE
                    habilitarEntrada(true)
                    Comun.mostrarMensaje(this@CreaIslaActiv, "La isla se creó con éxito", true)
                }

                override fun onError(anError: ANError?) {
                    //pbEsperaCreaIsla.visibility = View.INVISIBLE
                    habilitarEntrada(true)
                    when (anError?.errorCode) {
                        ERROR_INFO_INVALIDA -> {
                            Comun.mostrarMensaje(
                                this@CreaIslaActiv, MENSAJE_CREDENCIALES_INVALIDAS, true, true)
                        }
                        ERROR_NO_INTERNET -> {
                            Comun.mostrarMensaje(
                                this@CreaIslaActiv, MENSAJE_NO_INTERNET, true)
                        }
                        else -> {
                            Comun.mostrarMensaje(
                                this@CreaIslaActiv, MENSAJE_ERROR_GENERAL, true)
                        }
                    }
                }
            })
    }

    fun agregarAlias(v: View) {

        etNombreIslaNueva.isFocusableInTouchMode = false;
        etNombreIslaNueva.isFocusable = false;
        etNombreIslaNueva.isFocusableInTouchMode = true;
        etNombreIslaNueva.isFocusable = true;

        val etAlias = EditText(this)
        val dialogo = AlertDialog.Builder(this)
            .setMessage("Alias:\n")
            .setView(etAlias)
            .setPositiveButton("Aceptar", DialogInterface.OnClickListener() {
                    _: DialogInterface, _: Int ->
                val nuevo = etAlias.text.toString()
                arrAlias.add(nuevo)
                fragAlias.cambiarAdaptador(arrAlias)

                etNombreIslaNueva.isFocusableInTouchMode = false;
                etNombreIslaNueva.isFocusable = false;
                etNombreIslaNueva.isFocusableInTouchMode = true;
                etNombreIslaNueva.isFocusable = true;
            })
            .setNegativeButton("Cancelar", null)
            .setCancelable(false)
            .create()
        dialogo.show()
    }

    override fun clickOnIndex(posicion: Int, fragmento: ListFragment?) {
        // Preguntar si quiere borrar el alias
        val dialogo = AlertDialog.Builder(this)
            .setMessage("¿Quiere borrar el alias ${arrAlias[posicion]}?")
            .setNegativeButton("Si", DialogInterface.OnClickListener() {
                    _, _ ->
                arrAlias.removeAt(posicion)
                fragAlias.cambiarAdaptador(arrAlias)
            })
            .setPositiveButton("No",null)
            .create()
        dialogo.show()
    }

    private var entradaHabilitada = true

    private fun habilitarEntrada(habilitada: Boolean) {
        pbEsperaCreaIsla.visibility = if (habilitada) View.INVISIBLE else View.VISIBLE
        entradaHabilitada = habilitada
    }

    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        return if (entradaHabilitada) super.dispatchTouchEvent(ev) else true
    }
}
