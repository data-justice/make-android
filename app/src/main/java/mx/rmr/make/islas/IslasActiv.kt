package mx.rmr.make.islas

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.ListFragment
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import com.androidnetworking.interfaces.OkHttpResponseListener
import kotlinx.android.synthetic.main.activity_islas.*
import mx.rmr.make.*
import mx.rmr.make.botonpanico.ListenerFragmentoLista
import mx.rmr.make.util.Comun
import mx.rmr.make.util.ConceptoActiv
import okhttp3.Response
import org.json.JSONArray
import org.json.JSONObject

class IslasActiv : AppCompatActivity(), ListenerFragmentoLista
{
    private var arrIslasPendientes: JSONArray? = null
    private var arrIslasAceptadas: JSONArray? = null

    private lateinit var fragPendientes: ListaCadenasFrag
    private lateinit var fragAceptadas: ListaCadenasFrag

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_islas)

        fragPendientes = supportFragmentManager.findFragmentById(R.id.fragListaPendientes) as ListaCadenasFrag
        fragAceptadas = supportFragmentManager.findFragmentById(R.id.fragListaAceptadas) as ListaCadenasFrag

        descargarIslas()
    }

    private fun descargarIslas() {
        //pbEsperaIslas.visibility = View.VISIBLE
        habilitarEntrada(false)

        val prefs = getSharedPreferences(PREFS_CREDENCIALES, Context.MODE_PRIVATE)
        val token = prefs.getString(LLAVE_TOKEN, "")

        val direccion = URL_LISTA_MEMBERSHIPS

        AndroidNetworking.get(direccion)
            .addHeaders("Content-type","application/json; charset=utf-8")
            .addHeaders("Authorization", "Bearer $token")
            .build()
            .getAsJSONObject(object: JSONObjectRequestListener {
                override fun onResponse(response: JSONObject?) {
                    //pbEsperaIslas.visibility = View.INVISIBLE
                    habilitarEntrada(true)
                    arrIslasPendientes = response?.getJSONArray("pending")
                    mostrarIslasPendientes()
                    arrIslasAceptadas = response?.getJSONArray("active")
                    mostrarIslasAceptadas()
                }

                override fun onError(anError: ANError?) {
                    //pbEsperaIslas.visibility = View.INVISIBLE
                    habilitarEntrada(true)
                    when (anError?.errorCode) {
                        ERROR_NO_INTERNET -> {
                            Comun.mostrarMensaje(
                                this@IslasActiv, MENSAJE_NO_INTERNET
                            )
                        }
                        ERROR_INFO_INVALIDA -> {
                            Comun.mostrarMensaje(
                                this@IslasActiv, MENSAJE_CREDENCIALES_INVALIDAS, true, true)
                        }
                        else -> {
                            Comun.mostrarMensaje(
                                this@IslasActiv, MENSAJE_ERROR_GENERAL
                            )
                        }
                    }
                }
            })
    }

    private fun mostrarIslasAceptadas() {
        // ListFragment
        if (arrIslasAceptadas!=null && arrIslasAceptadas!!.length()>0) {
            val arrAceptadas = arrIslasAceptadas!!
            val arrIslas = mutableListOf<String>()
            for (i in 0 until arrAceptadas.length()) {
                val dIsla = arrAceptadas[i] as JSONObject
                val dGrupo = dIsla.getJSONObject("group")
                val nombreIsla = dGrupo.getString("name")
                arrIslas.add(nombreIsla)
            }

            val frag =
                supportFragmentManager.findFragmentById(R.id.fragListaAceptadas) as ListaCadenasFrag
            frag.cambiarAdaptador(arrIslas)
        } else {
            tvIslasAceptadas.text = "No tienes invitaciones pendientes"
        }
    }

    private fun mostrarIslasPendientes() {
        val arrIslas = mutableListOf<String>()
        if (arrIslasPendientes!=null && arrIslasPendientes!!.length()>0) {
            val arrPendientes = arrIslasPendientes!!
            for (i in 0 until arrPendientes.length()) {
                val dIsla = arrPendientes[i] as JSONObject
                val dGrupo = dIsla.getJSONObject("group")
                val nombreIsla = dGrupo.getString("name")
                arrIslas.add(nombreIsla)
            }
        } else {
            tvIslasPendientes.text = "No tienes invitaciones pendientes"
        }
        val frag =
            supportFragmentManager.findFragmentById(R.id.fragListaPendientes) as ListaCadenasFrag
        frag.cambiarAdaptador(arrIslas)
    }

    override fun clickOnIndex(posicion: Int, fragmento: ListFragment?) {
        when (fragmento) {
            fragPendientes -> {
                aceptarInvitacion(posicion)
                fragPendientes.listView.setItemChecked(posicion, false)
            }
        }
    }

    private fun aceptarInvitacion(posicion: Int) {
        // Preguntar
        val dialogo = AlertDialog.Builder(this)
            .setMessage("¿Aceptas la invitación para pertenecer a esta Isla de confianza?")
            .setNegativeButton("Si", DialogInterface.OnClickListener() {
                    _, _ ->
                subirAceptaInvitacion(posicion)
            })
            .setPositiveButton("No",null)
            .create()
        dialogo.show()
    }

    private fun subirAceptaInvitacion(posicion: Int) {
        val dIsla = arrIslasPendientes?.getJSONObject(posicion) ?: return

        habilitarEntrada(false)

        val id = dIsla.getInt("membership_id")

        val prefs = getSharedPreferences(PREFS_CREDENCIALES, Context.MODE_PRIVATE)
        val token = prefs.getString(LLAVE_TOKEN, "")

        val jsonBody = JSONObject("""{
            'pending': false
            }
        """.trimIndent())

        val direccion = URL_ACEPTA_INVITACION.replace(":id", "$id")

        AndroidNetworking.patch(direccion)
            .addHeaders("Content-type","application/json; charset=utf-8")
            .addHeaders("Authorization", "Bearer $token")
            .addJSONObjectBody(jsonBody)
            .build()
            .getAsJSONObject(object: JSONObjectRequestListener {
                override fun onResponse(response: JSONObject?) {
                    habilitarEntrada(true)
                    Comun.mostrarMensaje(this@IslasActiv, "Invitación aceptada")
                    descargarIslas()    // Refresca la isla aceptada
                }

                override fun onError(anError: ANError?) {
                    habilitarEntrada(true)
                    when (anError?.errorCode) {
                        ERROR_INFO_INVALIDA -> {
                            Comun.mostrarMensaje(
                                this@IslasActiv, MENSAJE_CREDENCIALES_INVALIDAS, true, true)
                        }
                        ERROR_NO_INTERNET -> {
                            Comun.mostrarMensaje(
                                this@IslasActiv, MENSAJE_NO_INTERNET)
                        }
                        else -> {
                            Comun.mostrarMensaje(
                                this@IslasActiv, MENSAJE_ERROR_GENERAL)
                        }
                    }
                }
            })
    }

    override fun longClickOnIndex(posicion: Int, fragmento: ListFragment?) {
        if (fragmento == fragAceptadas) {
            abandonarIsla(posicion)
        }
    }

    private fun abandonarIsla(posicion: Int) {
        // Abandonar la isla, Confirmar
        val dialogo = AlertDialog.Builder(this)
            .setMessage("¿Quieres abandonar esta Isla de confianza?")
            .setNegativeButton("Si", DialogInterface.OnClickListener() {
                    _, _ ->
                subirAbandonaIsla(posicion)
            })
            .setPositiveButton("No",null)
            .create()
        dialogo.show()
    }

    private fun subirAbandonaIsla(posicion: Int) {
        val dIsla = arrIslasAceptadas?.getJSONObject(posicion) ?: return

        val dGrupo = dIsla.getJSONObject("group")
        val esMiIsla = dGrupo.getBoolean("is_own")
        if (esMiIsla) {
            Comun.mostrarMensaje(this, "No puedes abandonar tu isla por defecto")
            return
        }

        //pbEsperaIslas.visibility = View.VISIBLE
        habilitarEntrada(false)

        val idIsla = dGrupo.getInt("id")

        val prefs = getSharedPreferences(PREFS_CREDENCIALES, Context.MODE_PRIVATE)
        val token = prefs.getString(LLAVE_TOKEN, "")
        val alias = prefs.getString(LLAVE_ALIAS, "")
        val idIslaGuardada = prefs.getInt(LLAVE_ID_ISLA_SELECCIONADA, 0)

        val direccion = URL_BORRAR_MIEMBRO.replace(":id", "$idIsla") + "/$alias"

        AndroidNetworking.delete(direccion)
            .addHeaders("Content-type","application/json; charset=utf-8")
            .addHeaders("Authorization", "Bearer $token")
            .build()
            .getAsOkHttpResponse(object: OkHttpResponseListener {
                override fun onResponse(response: Response?) {
                    //pbEsperaIslas.visibility = View.INVISIBLE
                    habilitarEntrada(true)
                    val codigo = response?.code() ?: return
                    when (codigo) {
                        OK_BORRAR_MIEMBRO -> {
                            Comun.mostrarMensaje(this@IslasActiv, "Has abandonado la isla de manera exitosa")
                            descargarIslas()    // Refrescar las islas
                            // Si es la isla seleccionada, borrarla de preferencias
                            if (idIsla==idIslaGuardada) {
                                prefs.edit().apply {
                                    putInt(LLAVE_ID_ISLA_SELECCIONADA, -1)
                                    putString(LLAVE_JSON_ISLA, "{}")
                                    commit()
                                }
                            }
                        }
                        ERROR_BORRAR_MIEMBRO_NO_EXISTE -> {
                            Comun.mostrarMensaje(this@IslasActiv, "No hay una isla con esta información")
                        }
                        ERROR_BORRAR_MIEMBRO_ACTIVO -> {
                            Comun.mostrarMensaje(this@IslasActiv, "No puedes abandonar esta isla porque hay un plan de seguridad activo con tu alias")
                        }
                    }
                }

                override fun onError(anError: ANError?) {
                    habilitarEntrada(true)
                    when (anError?.errorCode) {
                        ERROR_INFO_INVALIDA -> {
                            Comun.mostrarMensaje(
                                this@IslasActiv, MENSAJE_CREDENCIALES_INVALIDAS, true, true
                            )
                        }
                        ERROR_NO_INTERNET -> {
                            Comun.mostrarMensaje(
                                this@IslasActiv, MENSAJE_NO_INTERNET
                            )
                        }
                        else -> {
                            Comun.mostrarMensaje(
                                this@IslasActiv, MENSAJE_ERROR_GENERAL
                            )
                        }
                    }
                }
            })
    }

    fun mostrarRoles(v: View) {
        // Mandar el dMembership de la isla
        val posicion = fragAceptadas.indiceSeleccionado
        if (posicion!=null) {
            val dMembership = arrIslasAceptadas?.get(posicion) ?: return
            if (yaConcedio(LLAVE_CONCEPTO_ROL)) {
                val intRoles = Intent(this, RolesActiv::class.java)
                intRoles.putExtra(D_MEMBERSHIP, dMembership.toString())
                startActivity(intRoles)
            } else {
                val intConcepto = Intent(this, ConceptoActiv::class.java)
                intConcepto.putExtra(LLAVE_CONCEPTO_EXPLICA, LLAVE_CONCEPTO_ROL)

                val intRoles = Intent(this, RolesActiv::class.java)
                intRoles.putExtra(D_MEMBERSHIP, dMembership.toString())

                intConcepto.putExtra(LLAVE_SIGUIENTE_ACTIV, intRoles)
                startActivity(intConcepto)
            }
        } else {
            Comun.mostrarMensaje(this, "Selecciona una isla para definir roles")
        }
    }

    private fun yaConcedio(concepto: String): Boolean {
        val prefs = getSharedPreferences(PREFS_CONCEPTOS, Context.MODE_PRIVATE)
        return prefs.getBoolean(concepto, false)
    }

    private var entradaHabilitada = true

    private fun habilitarEntrada(habilitada: Boolean) {
        pbEsperaIslas.visibility = if (habilitada) View.INVISIBLE else View.VISIBLE
        entradaHabilitada = habilitada
    }

    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        return if (entradaHabilitada) super.dispatchTouchEvent(ev) else true
    }
}
