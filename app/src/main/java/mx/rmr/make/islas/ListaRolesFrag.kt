package mx.rmr.make.islas

import android.content.Context
import androidx.fragment.app.Fragment
import android.view.View
import android.widget.ArrayAdapter
import android.widget.ListView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.ListFragment
import mx.rmr.make.util.Comun

/**
 * A simple [Fragment] subclass.
 */
class ListaRolesFrag : ListFragment()
{
    // Renglones seleccionados
    private val MAX_ROLES = 3

    private var numeroSeleccionados = 0

    private val arrDatos = mutableListOf<String>()
    var contexto: Context? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        this.contexto = context
    }

    fun cambiarAdaptador(arrAlias: MutableList<Pair<Int,String>>?) {
        arrDatos.clear()

        if (arrAlias!=null) {
            for (alias in arrAlias) {
                arrDatos.add(alias.second)
            }

            val adapter = ArrayAdapter(contexto!!, android.R.layout.simple_list_item_multiple_choice, arrDatos)

            listAdapter = adapter
            listView.choiceMode = ListView.CHOICE_MODE_MULTIPLE
        }
    }


    override fun onListItemClick(l: ListView, v: View, position: Int, id: Long) {
        super.onListItemClick(l, v, position, id)

        val listener = contexto as ListenerListaRoles

        if (listView.isItemChecked(position)) {
            if (numeroSeleccionados == MAX_ROLES) {
                listView.setItemChecked(position, false)
                Comun.mostrarMensaje(contexto as AppCompatActivity,"Solo puedes seleccionar $MAX_ROLES roles")
            } else {
                numeroSeleccionados++
                listener.clickOnIndex(position, true)
            }
        } else { // Se apagó
            numeroSeleccionados--
            listener.clickOnIndex(position, false)
        }
    }

    fun prender(listaSeleccionados: List<Int>) {
        for(i in 0 until arrDatos.size) {
            if (listaSeleccionados.contains(i)) {
                listView.setItemChecked(i,true)
            } else {
                listView.setItemChecked(i,false)
            }
        }
        numeroSeleccionados = listaSeleccionados.size
    }

    fun apagar(indice: Int) {
        listView.setItemChecked(indice,false)
    }
}
