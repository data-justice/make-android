package mx.rmr.make.eventos

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.rv_renglon_evento.view.*
import mx.rmr.make.R

class AdaptadorEventos (private val contexto: Context, var arrEventos: Array<Evento>):
                        RecyclerView.Adapter<AdaptadorEventos.RenglonEvento>()
{
    // Para quitar la selección
    private var ultimoRenglon: RenglonEvento? = null
    var indiceSeleccionado: Int? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RenglonEvento {
        val vista = LayoutInflater.from(parent.context)
            .inflate(R.layout.rv_renglon_evento, parent, false)
        return RenglonEvento(vista)
    }

    override fun getItemCount(): Int {
        return arrEventos.size
    }

    override fun onBindViewHolder(holder: RenglonEvento, position: Int) {
        val evento = arrEventos[position]
        holder.vistaRenglon.tvTitulo.text = evento.titulo
        holder.vistaRenglon.tvDetalle.text = "${evento.alias}, ${evento.fecha}"

        if (evento.tipo==TipoRegistro.COMPLEMENTO) {
            holder.vistaRenglon.tvTitulo.text = "> ${evento.titulo}"
            val params = holder.vistaRenglon.linDatos.layoutParams as LinearLayout.LayoutParams
            params.leftMargin = 32
            holder.vistaRenglon.linDatos.layoutParams = params
            holder.vistaRenglon.tvTitulo.setTextColor(Color.BLUE)
            holder.vistaRenglon.tvDetalle.setTextColor(Color.BLUE)
            holder.vistaRenglon.btnDetalle.visibility = View.GONE
            // Los complementos se consultan directamente
            holder.vistaRenglon.setOnClickListener {
                val listener = contexto as ListenerConsultaEvento
                listener.btnClicked(position)
            }
        } else {    // EVENTO PRINCIPAL
            val params = holder.vistaRenglon.linDatos.layoutParams as LinearLayout.LayoutParams
            params.leftMargin = 0
            holder.vistaRenglon.linDatos.layoutParams = params

            val colorTexto = ContextCompat.getColor(contexto, android.R.color.darker_gray)
            holder.vistaRenglon.tvTitulo.setTextColor(colorTexto)
            holder.vistaRenglon.tvDetalle.setTextColor(colorTexto)
            holder.vistaRenglon.btnDetalle.visibility = View.VISIBLE
            // Evento info
            holder.vistaRenglon.isSelected = true
            holder.vistaRenglon.btnDetalle.setOnClickListener{
                val listener = contexto as ListenerConsultaEvento
                listener.btnClicked(position)
            }
            // Deja renglón para complemento (seleccionado)
            holder.vistaRenglon.setOnClickListener {
                holder.vistaRenglon.setBackgroundResource(R.color.azul_make_light)
                ultimoRenglon?.vistaRenglon?.setBackgroundResource(android.R.color.transparent)
                ultimoRenglon = holder
                // Avisa
                indiceSeleccionado = position
            }
            // Click largo para borrar el evento padre
            holder.vistaRenglon.setOnLongClickListener { v ->
                val listener = contexto as ListenerConsultaEvento
                listener.btnBorrarClicked(position, evento.idEvento)
                true
            }
        }
    }


    inner class RenglonEvento (var vistaRenglon: View): RecyclerView.ViewHolder(vistaRenglon)
    {
    }
}