package mx.rmr.make.eventos

interface ListenerConsultaEvento
{
    fun btnClicked(indice: Int)
    fun btnBorrarClicked(posicion: Int, idEvento: Int) { }  // Opcional
}
