package mx.rmr.make.eventos

enum class TipoEvento
{
    CAPTURA,
    CONSULTA,
    COMPLEMENTO
}
